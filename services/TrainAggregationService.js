import TrainInformationService from './TrainInformationService';
import TrainLocationService from './TrainLocationService';


export default TrainAggregationService = {
  getTrainsWithDistances: async (location) => {
    console.log("Aggregate trains and distances from", location);

    let start = new Date().getTime();

    let [locations, information] = await Promise.all([
      TrainLocationService.getTrainsWithDistances(location),
      TrainInformationService.getAllTrains()
    ]);
    console.log('total ' + (new Date().getTime() - start) / 1000);

    let infoMap = new Map( information.map(info => [info.trainNumber, info]) );

    console.log("Connecting locations to information", locations.length, information.length);

    return locations
      .filter(l => infoMap.get(l.trainNumber)) // skip if there is no information
      .map( location => ({
        ...infoMap.get(location.trainNumber),
        distance: location.distance
      }));
  }
};
