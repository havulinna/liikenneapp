const STATIONS_URL = "https://rata.digitraffic.fi/api/v1/metadata/stations";

const HEADERS = new Headers({ 'Content-Type': 'application/json' });

/*
EXAMPLE station from api:
{
  "passengerTraffic": true,
  "type": "STATION",
  "stationName": "Pasila asema",
  "stationShortCode": "PSL",
  "stationUICCode": 10,
  "countryCode": "FI",
  "longitude": 24.93352100000000,
  "latitude": 60.19868900000000
}*/

let removeSuffix = (station) => ({
  ...station,
  stationName: station.stationName.replace(new RegExp(' asema$'), '')
});


export default StationService = {
  _stationPromise: null,

  getStations: async () => {
    if (StationService._stationPromise === null) {
      // Cache results in module variable
      StationService._stationPromise = fetch(STATIONS_URL, { method: 'GET', headers: HEADERS })
        .then(res => res.json())
        .then(stations => stations.map(removeSuffix))
        .catch(error => {
          console.log('Error: ', error);
          // Clear the cached errorneous response
          StationService._stationPromise = null;
          throw e;
        });
    }
    return StationService._stationPromise;
  },

  getStationsMap: async () => {
    let stations = await StationService.getStations();

    return new Map( stations.map(s => [s.stationShortCode, s]) );
  }
}
