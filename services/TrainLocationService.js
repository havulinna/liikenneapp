import geolib from 'geolib';

const LIVE_TRAINS_URL = 'https://rata.digitraffic.fi/api/v1/train-locations/latest/';
const HEADERS = new Headers({ 'Content-Type': 'application/json' })

function addDistance(location) {
  return function transformTrain(train) {
    let latLng = { latitude: train.location.coordinates[1], longitude: train.location.coordinates[0] };
    let distance = geolib.getDistance(location, latLng);

    return {
      ...train,
      distance: distance
    };
  }
}

export default TrainLocationService = {
  /* Returns custom train location objects with distances from closest to furthest.
  * example location: { latitude: 0, longitude: 0 }
  */
  getTrainsWithDistances: async (location) => {
    console.log("Get trains with distance from", location);
    let trains = await fetch(LIVE_TRAINS_URL, { method: 'GET', headers: HEADERS })
      .then(res => res.json());
    return trains.map(addDistance(location)).sort((a, b) => a.distance - b.distance );
  }
}
