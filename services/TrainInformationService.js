import StationService from './StationService';
import _ from 'lodash';

const TRAINS_URL = 'https://rata.digitraffic.fi/api/v1/live-trains/';
const SINGLE_TRAIN_URL = 'https://rata.digitraffic.fi/api/v1/trains/latest/<train_number>?version=<version>';

const HEADERS = new Headers({ 'Content-Type': 'application/json' });

function filterTimeTable(rows, stations) {
  rows = rows
    .filter(row => row.trainStopping)
    .map(row => ({
      ...row,
      station: stations.get(row.stationShortCode)
    }));

  let airport = rows.find( r => r.stationShortCode === 'LEN' && r.type === 'DEPARTURE');
  if (!airport || _.last(rows) === airport || _.first(rows) === airport) {
    return rows;
  } else {
    let airportIndex = rows.indexOf(airport);

    // Has the train reached airport?
    if (!airport.actualTime) {
      return rows.slice(0, airportIndex);
    } else {
      return rows.slice(airportIndex);
    }
  }
}

function transformTrain(stations) {
  return t => {
    let timeTable = filterTimeTable(t.timeTableRows, stations);
    return {
      commuterLineID: t.commuterLineID,
      trainNumber: t.trainNumber,
      trainType: t.trainType,
      trainCategory: t.trainCategory,
      version: t.version,
      cancelled: t.cancelled,
      runningCurrently: t.runningCurrently,
      departureStation: stations.get(timeTable[0].stationShortCode),
      destinationStation: stations.get(timeTable[timeTable.length-1].stationShortCode),
      timeTable: timeTable // TODO
    }
  }
}

/**
 * Train information service will request live information about trains from Liikennevirasto.
 * It will also add Station objects to the timeTableRows as well as filter non-stopping stations
 * from the array.
 */
export default TrainInformationService = {
  getAllTrains: async () => {
    console.log('Get all trains');

    let [trains, stations] = await Promise.all([
      fetch(TRAINS_URL, { method: 'GET', headers: HEADERS }).then(res => res.json()).catch(e => console.log(e)),
      StationService.getStationsMap()
    ]);

    return trains.map( transformTrain(stations) );
  },

  updateTrain: async (train) => {
    let url = SINGLE_TRAIN_URL.replace('<train_number>', train.trainNumber).replace('<version>', train.version);
    console.log('get train ' + train.trainNumber, url);

    let [trains, stations] = await Promise.all([
      fetch(url, { method: 'GET', headers: HEADERS }).then(res => res.json()),
      StationService.getStationsMap()
    ]);

    console.log(trains.length ? 'train was updated' : 'train was not updated');
    return trains.length ? trains.map( transformTrain(stations) )[0] : train;
  }
}
