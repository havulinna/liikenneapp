import React, { Component } from 'react'
import { View, ScrollView, Alert } from 'react-native'
import { Icon, Text, List, ListItem, Header } from 'react-native-elements'
import { Constants, Location, Permissions } from 'expo';
import _ from 'lodash';
import moment from 'moment';
import TrainInformationService from '../services/TrainInformationService';

const SILVER = '#cccccc';
const BLUE = '#2089dc';
const GREEN = '#43a047';
const BLACK = '#111111';
const UPDATE_INTERVAL = 10000; // milliseconds

class StationView extends Component {

  static navigationOptions = {
      header: null
  }

  state = { train: null, nextStop: null, currentStop: null };
  interval = null;
  mounted = false;

  componentWillMount = async () => {
    this.setState({ train: this.props.navigation.state.params.train })
  }

  componentDidMount() {
    this.mounted = true;
    this.loadData();
    this.timer = setInterval(() => this.loadData(), UPDATE_INTERVAL);
  }

  componentWillUnmount() {
    this.mounted = false;
    clearInterval(this.timer);
  }

  loadData = async () => {
    console.log('fetch new data');

    let train = this.state.train;

    try {
      train = await TrainInformationService.updateTrain(train);
    } catch(e) {
      console.log(e);
      Alert.alert('Updating timetable failed', e.toString(),
        [ {text: 'Ok'} ]
      );
    }

    let currentStop = this._getCurrentStop(train.timeTable);
    let nextStop = train.timeTable.find(s => !s.actualTime && s.type === 'ARRIVAL');

    if (this.mounted) {
      this.setState({ train: train, nextStop: nextStop, currentStop: currentStop });
    }
  }

  _getCurrentStop(timeTable) {
    // The stop where the train last arrived to
    let lastArrival = _.last(timeTable.filter(s => s.actualTime && s.type === 'ARRIVAL'));
    let endStation = _.last(timeTable);

    if (lastArrival === endStation) {
      return endStation; // The train is already at the end station
    } else if (lastArrival) {
      // Has the train departed the station it last arrived to?
      let departure = timeTable.find( stop => stop.type === 'DEPARTURE' && stop.stationShortCode === lastArrival.stationShortCode );
      return (departure && !departure.actualTime) ? lastArrival : null;
    } else {
      // Has the train even left the first station?
      return timeTable[0].actualTime ? null : timeTable[0];
    }
  }

  render = () => {
    let train = this.state.train;

    return (<View>
      <ScrollView>
        <Header
          leftComponent={
            <Icon name='chevron-left' color='#ffffff' onPress={ () => this.props.navigation.goBack() } />
          }
          centerComponent={{ text: this._trainTitle(train) + '     ' + this._stationNames(train), style: { color: '#fff' } }}
          rightComponent={
            <Icon name='info' color='#ffffff' onPress={ () => this.props.navigation.navigate('License') } />
          }
        />
        <List style={{ marginTop:  0, marginBottom: 20 }}>
          {
            train.timeTable.filter((stop, i) => i === 0 || stop.type === 'ARRIVAL').map((trainStop, i) => (
              <ListItem
                leftIcon={ {name: 'train', color: this._iconColor(trainStop) } }
                key={ this._generateKey(trainStop) }
                title={ trainStop.station.stationName }
                titleStyle={ this._titleStyle(trainStop) }
                subtitle={ this._stationTime(trainStop) }
                hideChevron={ true }
              />
            ))
          }
        </List>
      </ScrollView>
    </View>);
  }

  _trainTitle(train) {
    return train.commuterLineID ? train.commuterLineID : (train.trainType + '' + train.trainNumber);
  }

  _stationNames(train) {
    return _.first(train.timeTable).station.stationName + ' - ' + _.last(train.timeTable).station.stationName;
  }

  _stationTime(stop) {
    let scheduled = this._formatTime(stop.scheduledTime);
    let updated = this._formatTime(stop.actualTime || stop.liveEstimateTime || stop.scheduledTime);
    let style = { fontSize: 12, fontWeight: 'normal', color: this._stationColor(stop) };

    // is the train in schedule?
    if (scheduled !== updated) {
      return (<Text style={ style }>
        <Text style={{ textDecorationLine: 'line-through' }}>{scheduled}</Text> ⇨ {updated}
      </Text>);
    } else {
      return (<Text style={ style }>{ scheduled }</Text>);
    }
  }

  _formatTime(timeStr) {
    return moment(timeStr).format('k:mm');
  }

  _iconColor(stop) {
    if (stop === this.state.currentStop) {
      return GREEN;
    } if (stop.actualTime) {
      return SILVER;
    } else {
      return BLUE;
    }
  }

  _stationColor(stop) {
    if (stop === this.state.currentStop) {
      return "green";
    } if (stop.actualTime) {
      return SILVER;
    } else {
      return BLACK;
    }
  }

  _titleStyle(stop) {
    return {
      color: this._stationColor(stop),
      fontWeight: stop === this.state.currentStop ? 'bold' : 'normal'
    };
  }

  _generateKey(stop) {
    let isNext = this.state.nextStop === stop;
    return stop.stationShortCode + stop.scheduledTime + (isNext ? 'next' : '');
  }
}


export default StationView;
