import React, { Component } from 'react';
import { View, ScrollView, Alert, Image } from 'react-native';
import { Avatar, Icon, Text, Button, List, ListItem, Header } from 'react-native-elements';
import { Location, Permissions } from 'expo';
import TrainAggregationService from '../services/TrainAggregationService';
import _ from 'lodash';
import moment from 'moment';

const DEFAULT_LOCATION = {latitude: 60.1699, longitude: 24.9384}; // Pasila
const BLUE = '#2089dc';
const GREEN = '#43a047';
const MAX_TRAINS = 10;

class SelectTrain extends Component {
  static navigationOptions = {
      header: null
  }

  state = { trains: [], status: 'Initializing...', loading: false };

  componentDidMount = async () => {
    this.locateTrains();
  }

  getLocation = async () => {
    let permission = await Permissions.askAsync(Permissions.LOCATION);
    if (permission.status !== 'granted') {
      console.log("no permission");
      Alert.alert(
        'No location',
        'This app requires access to your location to track the train you are in. To fully utilise the app please allow us to access location services.',
        [ {text: 'OK'} ]
      )
      return null;
    }
    return (await Location.getCurrentPositionAsync({})).coords;
  }

  locateTrains = async () => {
    try {
      this.setState({ status: 'Waiting for location...', trains: [], loading: true });

      let location = (await this.getLocation()) || DEFAULT_LOCATION;

      this.setState({ status: 'Locating nearby trains...' });

      let trains = await TrainAggregationService.getTrainsWithDistances(location);

      trains = trains.filter(t => t.trainCategory !== 'Cargo').splice(0, MAX_TRAINS);
      this.setState({ status: 'We found the following trains near your location:', trains: trains, loading: false });

      if (trains.length && location !== DEFAULT_LOCATION) {
        Alert.alert('Is this your train?', this._trainTitle(trains[0]) + '\n' + this._stationNames(trains[0]),
          [ {text: 'Yes', onPress: () => this._selectTrain(trains[0])}, {text: 'No'} ]
        );
      }
    } catch(e) {
      console.log(e);
      Alert.alert('Could not locate the nearest trains', e.toString(),
        [ {text: 'Ok'} ]
      );
      this.setState({ status: 'Locating nearest trains failed :(', trains: [], loading: false });
    }
  }

  render = () => {
    return (<View>
      <ScrollView>
        <Header
          leftComponent={
            <Icon name='info' color='#ffffff' onPress={ () => this.props.navigation.navigate('License') } />
          }
          centerComponent={{ text: 'Select your train', style: { color: '#fff' } }}
          rightComponent={
            <Icon name='refresh' color='#ffffff' onPress={ () => this.locateTrains() } />
          }
        />

        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <Text style={ { marginTop: 10, marginLeft: 10, marginBottom: 10, marginRight: 10} }>
            { this.state.status }
          </Text>
          { this._loadingIndicator() }
          { this._reloadButton() }
        </View>

        <List style={{ marginTop:  0, marginBottom: 20 }}>
          {
            this.state.trains.map((train, i) => (
              <ListItem
                onPress={ () => this._selectTrain(train) }
                //leftIcon={ {name: 'train'} }
                avatar={
                  <Avatar small rounded title={ this._avatarText(train) } overlayContainerStyle={ this._avatarStyle(train) } />
                }
                rightTitle={ this._distanceText(train.distance) }
                rightTitleStyle={{ color: this._distanceColor(train.distance) }}
                key={ train.trainNumber }
                title={ this._stationNames(train) }
                subtitle={ this._trainSchedule(train) }
              />
            ))
          }
        </List>
      </ScrollView>
    </View>);
  }

  _loadingIndicator() {
    if (this.state.loading) {
      return <Image source={require('../images/loading.gif')} />
    }
  }

  _reloadButton() {
    if (!this.state.loading && !this.state.trains.length) {
      return <Button
        backgroundColor={ BLUE }
        raised
        icon={{name: 'refresh'}}
        title='Locate trains'
        onPress={ () => this.locateTrains() } />
    }
  }

  _trainTitle(train) {
    return train.commuterLineID ? train.commuterLineID : (train.trainType + ' ' + train.trainNumber);
  }

  _avatarText(train) {
    return train.commuterLineID ? train.commuterLineID : train.trainType;
  }

  _avatarStyle = (train) => ({ backgroundColor: train.commuterLineID ? BLUE : GREEN });

  _stationNames(train) {
    return _.first(train.timeTable).station.stationName + ' - ' + _.last(train.timeTable).station.stationName;
  }

  _distanceText(distance) {
    return distance < 500 ? 'Very close' : (distance < 1000 ? 'Nearby' : undefined);
  }

  _distanceColor(distance) {
    return distance < 500 ? 'green' : distance < 1000 ? 'orange' : 'red';
  }

  _trainSchedule(train) {
    let firstStop = _.first(train.timeTable);
    let lastStop = _.last(train.timeTable);
    let formatTime = (stop) => moment(stop.scheduledTime).format('k:mm');

    return  formatTime(firstStop) + ' - ' + formatTime(lastStop);
  }

  _selectTrain(train) {
    const navigation = this.props.navigation;
    navigation.navigate('TrainStations', { train: train });

    //this.setState({ status: 'Please press reload to locate trains.', trains: [], loading: false });
  }
}


export default SelectTrain;
