import React, { Component } from 'react'
import { View, ScrollView, Alert, Linking } from 'react-native'
import { Icon, Text, List, ListItem, Header } from 'react-native-elements'
import { Constants, Location, Permissions } from 'expo';

const SILVER = '#cccccc';
const BLUE = '#2089dc';
const GREEN = '#43a047';

const LINK_STYLE = { color: BLUE };

class LicenseView extends Component {
  static navigationOptions = {
      header: null
  }

  state = { };

  render = () => {
    let train = this.state.train;

    return (
      <View>
        <ScrollView>
          <Header
            leftComponent={
              <Icon name='chevron-left' color='#ffffff' onPress={ () => this.props.navigation.goBack() } />
            }
            centerComponent={{ text: 'Information', style: { color: '#fff' } }}
          />
          <View style={{ paddingLeft: 10, paddingTop: 10, paddingRight: 10 }}>
            <Text style={{ paddingBottom: 10 }}>
              The information displayed on this application is provided by the Finnish Transport Agency
              through their <Text
                onPress={ () => this.openDigitraffic() } style={ LINK_STYLE }>
              rata.digitraffic.fi
            </Text> service.
            </Text>
            <Text>
            The data is licensed with a <Text onPress={ () => this.openLicense() } style={ LINK_STYLE }>CC 4.0 BY</Text> license.
            </Text>
          </View>
        </ScrollView>
      </View>);
  }

  openDigitraffic() {
    console.log('open digitraffic');
    Linking.openURL('https://rata.digitraffic.fi');
  }
  openLicense() {
    console.log('open license');
    Linking.openURL('https://creativecommons.org/licenses/by/4.0/');
  }
}


export default LicenseView;
