import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import SelectTrain from './components/SelectTrain';
import StationsView from './components/StationsView';
import LicenseView from './components/LicenseView';
import { StackNavigator } from 'react-navigation';

export default App = StackNavigator({
  Home: { screen: SelectTrain },
  TrainStations: { screen: StationsView },
  License: { screen: LicenseView },
});
